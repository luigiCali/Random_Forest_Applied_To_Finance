#--------------------------------------------------------
#---------------------LIBRERIE---------------------------
#--------------------------------------------------------
rm(list = ls()) # remove previous objects
setwd("C:/Users/admin/Desktop/CorsoData science/Modulo_9/progetto_finale/progetto_finale")
source("funzioni.R")
library(MASS)                 
library(stats)
library(rpart)
library(caret)
library(gbm)
library(randomForest)
library(ROCR) 
library(rpart.plot)
library(Amelia)
library(pscl)
library("e1071")
library("ada")
library(ROCR)
library(pROC)



library(adaStump)
#--------------------------------------------------------
#---------------------FILE CSV---------------------------
#--------------------------------------------------------
#--------------------------------------------------------

df=read.csv("dataset.csv",sep=";",header=T)
colnames(df)

#Leggiamo il file csv, poi chiediamo di
#il nome delle colonne del dataframe.


#--------------------------------------------------------
#--------------------OCCORRENZE TARGET--------------------
#--------------------------------------------------------
occorrenze(df$TARGET,1,0,0.7) 


#Per prima cosa vediamo se il TARGET
#ossia la colonna per cui vogliamo 
#classificare sia "equilibrata" ossia 
#che abbia un rapporto tra classificati
#nella classe 1 con quelli classificati
#nella classe 2 o viceversa, almeno pari
#a 0.7. Nel nostro � pari a 1, quindi
#� perfettamente in equilibrio.




#--------------------------------------------------------
#--------------------VALORI COSTANTI---------------------
#--------------------------------------------------------
k=valoricostanti(df,0.85)
#Le colonne che hanno una frequenza di un
#loro elemento che si ripete piu del 85%
#saranno inserite in un vettore e poi 
#droppate.
df=df[,-k]

#--------------------------------------------------------
#--------------------PICCOLA MODIFICA--------------------
#--------------------------------------------------------

plot(df$var36)                        #ante-modifica

df$var36[df$var36==99]=4         #Nella colonna var36, ci sono dei valori
#pari a 99 che danno fastidio, inseriamo una quarta
#modalit�


plot(df$var36)                      #post-modifica      


#--------------------------------------------------------
#--------------------DOPPIONI------------------------
#--------------------------------------------------------


k=trova_doppioni(df,0.99)

df=df[,-k]



#Questa funzione procede a indivuare e poi
#a droppare le colonne che sono simili al
#99%
#--------------------------------------------------------
#--------------VAR TROPPO INFORMATIVE--------------------
#--------------------------------------------------------
df=variabili_informative(df,0.98)


#Vengono droppate le variabili che presentono
#valori distinti almeno per il 98% del loro
#contenuto, come ad esempio ID o il codice
#fiscale dato che non permettono di poter
#generalizzare.
#--------------------------------------------------------
#--------------------CONTROLLO NA------------------------
#--------------------------------------------------------

#grafico di missing
missmap(df, main = "Missing values vs observed")
#con bianco si ha missing, con rosso scuro no

t=contaNA(df,-999999,9999999999)

df=correggiNA(df,t,-999999,9999999999,"mean")

#La funzione NA conta e vede dove sono presenti
#i NA considerando anche i -999999 e 9999999999
#come NA. Tuttavia quest'ultimi erano presenti
#nelle colonne che gi� sono state droppate nel
#punto precedente. La correzione dei NA pu� 
#avvenire con "mean" o con "median"


#--------------------------------------------------------
#--------------------CONTROLLO OUTLIER-------------------
#--------------------------------------------------------


#df=correggi_outlier(df,1,"extreme")        

#Questa funzione procede a correggere gli outlier
#considerandoli tali se inferiori a q1-k(q3-q1)
#o superiori a q3+k(q3-q1), dove q1 � il primo
#quantile, q3 il terzo e k un parametro dove
#determina l'ampiezza di questi intervallo, piu
#e grande e piu considera meno gli outlier. Nel
#nostro caso � stato considerato pari a 1. SI
#pu� correggere gli outliers con "mean", "median"
#oppure con "extreme" dove metter� q1 se l'outlier
#inferiore di q1 oppure q3 se superiore del terzo
#quantile.





#--------------------------------------------------------
#--------------------VARIE MODIFICHE---------------------
#--------------------------------------------------------

as.factor(df$ind_var5)
as.factor(df$TARGET)
as.factor(df$var36) 
#Mettiamo come factor alcune variabili


#--------------------------------------------------------
#--------------------TABELLE DI CONTINGENZA %------------
#--------------------------------------------------------

tab1=table(df[,3],df$TARGET)
round(prop.table(tab1), 2)*100
#Permettono di vedere come al variare
#delle modalit� della variabile df[,5]
#venga associato alla classe 0 o 1
#del target. Tuttavia non � un buon
#strumento nel caso di molte variabili


#--------------------------------------------------------
#-----------------CORRELAZIONI--------------------------
#--------------------------------------------------------

cor(df[,-ncol(df)],df$TARGET)*100

#vediamo le correlazioni percentuali
#delle variabili con il target

k=corr_var(df,TARGET,3)
df=df[,-k]                   #elimina le variabili che sono correlate
#meno del 3% con il target
#--------------------------------------------------------
#--------------------SPLIT DATASET-----------------------
#--------------------------------------------------------
df_split=split_dataset(df,0.632,"no seed")
training = df_split$training
testing = df_split$testing              
#Splitta il dataset, 67% in training set
#e il restante 33% nel test set. Non
#viene utilizzato alcun seme per i
#numeri casuali, nel caso inserire il
#numero senza " " tipo 372638

#--------------------------------------------------------
#--------------------DECISION TREE-----------------------
#--------------------------------------------------------

#creo l'albero
tree = rpart(factor(training$TARGET) ~ ., data = training, method ='class')

#informazioni dell'albero

summary(tree)       
#effettuo la predizione
pred.tree = predict( tree, testing[,-ncol(testing)], type="class" ) 

#matrice confusione dell'albero
conf_matr=confusion_matrix(pred.tree,testing$TARGET)
attach(conf_matr)
tbt_tree=confmatrix

tree_val=c(accuracy,sensitivity,specificity)

detach(conf_matr)

#plot dell'albero
rpart.plot(tree)            
#vediamo che ci sono 4 nodi, possiamo potarli.



#--------------------------------------------------------
#--------------------DECISION TREE PRUNE-----------------
#--------------------------------------------------------

#potatura
#la potatura serve per evitare l'overfitting dell'albero
#decisionale, ossia predice il rumore, ossia la parte
#irrelevanti dei dati. In questo caso la prestazione
#sul test set e nettamente superiore che sul training set.
#Ci sono alcune variabili irrivelanti che devono
#essere tolte. Per fare questo potiamo l'albero decisionale.
#Si usa un particolare test chiquadro per individuale.




printcp(tree)    #veduani i valori CP che permettono di potare l'albero
#nei vari nodi.

plotcp(tree)   #Nelle y abbiamo l'errore del Test atteso che si genera
#dall'albero. Si ricorda che vogliamo potarlo proprio per
#il fatto che possiamo avere un buona predizione nel training
#set ma una piu peggiore predizione nel test set, questo
#per il problema dell'overfitting.

#Il valore per potare deriva dal fatto di partire dal basso
#vediamo per lo split quanti sono stati attribuiti correttamente
#e quanti no. Ad esempio se l'errore � del 10% noi stabiliremo
#un valore cp tale per cui se inferiore a 10% non poteremo il
#primo split, se invece superiore ad esempio poteremo il
#primo ma anche il secondo o il terzo se superiore a quelli.

#scelta ottimale della potatura
CP_best=tree$cptable[which.min(tree$cptable[,"xerror"]),"CP"]



tree_prune = rpart(training$TARGET~ .,data=training, method ='class', cp=CP_best)

#plot albero
rpart.plot(tree_prune)

#pred dell'albero potato
pred.tree_prune = predict(tree_prune, testing[,-ncol(testing)], type="class" )

#matrice confusione albero potato
conf_matr=confusion_matrix(pred.tree_prune,testing$TARGET)
attach(conf_matr)
tbt_tree=confmatrix
tree_prune_val=c(accuracy,sensitivity,specificity)

detach(conf_matr)






#inseriamo i valori ottenuti in un dataframe
valori=data.frame(tree_val,tree_prune_val)  
rownames(valori)=c("Acc","Sen","Spe")




#--------------------------------------------------------
#--------------GRADIENT BOOSTING MACHINE-----------------
#--------------------------------------------------------

#Questo modello consiste nell'ensemble di classificatori deboli,
#in particolare di decision tree. La variabile di classificazione
#y* che si ricorda in questo caso in grado di assumere valore 0 o 1,
#pu� essere visto come una funzione F dell'input, che descrive 
#un classificatore y*=F(x). Ovviamente commetter� l'errore, dato
#il mean-square error (y*-y)^2 si vuole minimizzarlo. Per fare questo
#Si aggiunge un nuova variabile F+1(x)=y*=F(x)+h(x). Per trovarlo
#per prima cosa si fa h(x)=y-F(x). Noi fitteremo h(x) considerando
#questa equazione. Si aggiunger� nuovi variabili sempre con l'idea
#di minimizzare l'errore commesso.

#modello

gbmfit =  gbm(training$TARGET ~ ., data=training, distribution = "bernoulli", 
              shrinkage = .1, # parameter v (learning rate, 0 < v < 1 = lower learning rate requires more iterations: uses 0.1 for all data sets with more than 10,000 records)
              bag.fraction = 0.5,  # parameter eta (can use fraction greater than 0.5 if training sample is small)
              cv.folds = 5, n.trees = 500)        # parameter M



#Seleziona il numero ottimale di iterazioni

dev.off()
windows()
select.M.iter = gbm.perf(gbmfit,method="cv")


#Informazioni in base agli alberi adottati
summary(gbmfit,n.trees=1)             # based on the first tree
summary(gbmfit,n.trees=select.M.iter) # based on the estimated M (cv)

#predizione
pred.test.gbm = predict(gbmfit,  testing[,-ncol(testing)], type="response", n.trees=select.M.iter )

#istogramma
hist(pred.test.gbm,30)

#dato che non fornisce valori pari a 0 o 1 interi,
#li si arrotonda considerando 0.5 come valore di
# riferimento. Se maggiore di 0.5 allora 1 altrimenti
#0
pred.gbm = as.numeric(pred.test.gbm > .5 ) # classifier 


#matrice di confusione
conf_matr=confusion_matrix(pred.gbm,testing$TARGET)
attach(conf_matr)
tbt_tree=confmatrix
gbm_val=c(accuracy,sensitivity,specificity)

detach(conf_matr)

valori[[3]]=gbm_val
colnames(valori)=c("tree","tree_pru","gbm")



#--------------------------------------------------------
#----------------------RANDOM FOREST---------------------
#--------------------------------------------------------

#Cerca il numero ottimale di alberi per la foresta

mtry <- tuneRF(training,training$TARGET, ntreeTry=500,
               stepFactor=1.5,improve=0.01, trace=TRUE, plot=TRUE)

best.m <- mtry[mtry[, 2] == min(mtry[, 2]), 1]

print(mtry)



rf = randomForest(as.factor(training$TARGET) ~., data=training,
                  ntree=500,
                  mtry = best.m, # number of variables sampled 
                  importance=TRUE,na.action=na.roughfix)

#na.action indica che eventuali NA sono corretti
#con la mediana nel caso di valori quantitativi o
#con la moda nel caso di fattoriali


#miglior modo per trattare i NA, tuttavia non ci sono
#rf.imputed <- rfImpute(as.factor(training$TARGET) ~ ., data=training)
#rf = randomForest(as.factor(training$TARGET) ~., data=rf.imputed,
#                  ntree=500,
#                  mtry = 12, # number of variables sampled 
#                  importance=TRUE)


#Info sulla random forest
print(rf)







#fornisce un albero della Foresta
getTree(rf,k = 22)
#predizione
pred.rf = predict( rf, newdata=testing[,-ncol(testing)], type='prob')[,2]

pred.rf = as.numeric(pred.rf > .5 ) # classifier
#matrice di confusione
conf_matr=confusion_matrix(pred.rf,testing$TARGET)
attach(conf_matr)
tbt_tree=confmatrix
rf_val=c(accuracy,sensitivity,specificity)

detach(conf_matr)

valori[[4]]=rf_val
colnames(valori)=c("tree","tree_pru","gbm","randFor")


#fornisce quali variabili sono piu importanti

importance(rf,type=NULL, class=NULL, scale=TRUE) 

#importanza grafico, si vede come riducono
#il Gini mediamente in ogni albero per ciascuna
#variabile

varImpPlot(rf,type=2, class=NULL, scale=TRUE, top=10,sort = TRUE)


#Prendiamo le prime 7
vett=NULL
u=1
Imporance_feature=c("var15","var38","saldo_var30","saldo_var42","saldo_medio_var5_hace3","saldo_medio_var5_ult3","saldo_medio_var5_hace2","imp_op_var41_ult1","TARGET")
for(imp in Imporance_feature){
  k=grep(imp,colnames(df))            #dato il nome della colonna ne fornisce l'indice
  vett[u]=k
  u=u+1
}

df_imp=df[,vett]                        #dato questo nuovo dataframe 
#lo utillizziamo di nuovo per i
#modelli visti






#--------------------------------------------------------
#--------------------------GLM---------------------------
#--------------------------------------------------------




# ln(odds) = ln(p/(1-p)) = a*x1 + b*x2 + . + z*xn
LG_full = glm(as.factor(training$TARGET) ~.,  family=binomial(link="logit"), data= training);

#informazioni sul modello
summary(LG_full);
#Le stelle vicino alle variabili indicano quando sono importanti
#le variabili. In questo caso sono l'intercetta, la var15, imp_op_var41_ult1,
#num_var30, num_meses_var5_ult3,var38 e meno significativit� 
#hanno num_var42,saldo_var5,saldo var42,var36,num_meses_var5_ult3....
#La significativi� si vede con il p-value.

anova(LG_full, test="Chisq")
#Analisi della tabella delle devianze. vede come risponde il modello
#nel caso di modello nullo ossia solo con intercetta. Ci sono variabili poco
#significative che se le togliamo il modello spiega la stessa variazione.

#per vedere la bont� del modello, si utilizza McFadden R^2.

pR2(LG_full)
#Possiamo vedere come il modello non � particolarmente buono


#predizione
pred.glm_0=predict(LG_full,newdata=testing,type='response')
# output probabilities in the form of P(y=1|X).


pred.glm <- ifelse(pred.glm_0 > 0.5,1,0)
#Arrotonda i valori maggiori di 0.5 in 1 e gli altri
#mette 0


#matrice di confusione
conf_matr=confusion_matrix(pred.glm,testing$TARGET)
attach(conf_matr)
tbt_tree=confmatrix
glm_val=c(accuracy,sensitivity,specificity)

detach(conf_matr)

valori[[5]]=glm_val
colnames(valori)=c("tree","tree_pru","gbm","randFor","glm")




#--------------------------------------------------------
#--------------------------SVM---------------------------
#--------------------------------------------------------

#permette di fare regressione e anche classificazione.
#Classifica in modo non lineare, formando partizione
#dei dati non linearmente. Si cerca l optimal separating
#hyperplane dove � la retta che separa in parti uguali i dati.
# A  |B non � ottimale l'ottimale separa in parti uguali
# A | B
#
# h(x)=w^{T}x+b=w_{1}x_{1}+w_{2}x_{2}+...+w_{d}x_{d}+b
#Dove b � un bias e gli w sono pesi. Questa � l'equazione
#che deve splittare in due parti l hyperplane. Se si pu�
#separare con una retta lineare, vuol dire che possiamo
#trovare una retta che separa tutti gli A con tutti i B.

#Cerchiamo parametri ottimale per la SVM
#attach(training)
#tuned <- tune.svm(TARGET~., data = training, gamma = 10^(-6:-1), cost = 10^(1:2))
#detach(training)   #sono commentati perch� impiega molto tempo

#summary(tuned)     #vediamo informazioni sui parametri ottimali per metterli
#nel modello



svm_model <- svm(training$TARGET ~ ., data=training,cost = 100, gamma = 1)

#info sul modello
summary(svm_model)


#predizione
pred.svm <- predict(svm_model,newdata=testing,type="response")
pred.svm <- ifelse(pred.svm > 0.5,1,0)

#matrice di confusione
conf_matr=confusion_matrix(pred.svm,testing$TARGET)
attach(conf_matr)
tbt_tree=confmatrix
svm_val=c(accuracy,sensitivity,specificity)

detach(conf_matr)

valori[[6]]=svm_val
colnames(valori)=c("tree","tree_pru","gbm","randFor","glm","svm")




#--------------------------------------------------------
#---------------------ADA BOOSTING-----------------------
#--------------------------------------------------------


fit.ada <- ada(training$TARGET ~ .,data = training, type = "real",
               control = rpart.control(maxdepth=1,cp=-1,minsplit=0,xval=0)
               , iter = 40, nu = 0.05, bag.frac = 1)




pred.ada <- predict(fit.ada, testing,type = "prob")
pred.ada <- ifelse(pred.ada > 0.5,1,0)[,2]




#matrice di confusione
conf_matr=confusion_matrix(pred.ada,testing$TARGET)
attach(conf_matr)
tbt_tree=confmatrix
ada_val=c(accuracy,sensitivity,specificity)

detach(conf_matr)

valori[[7]]=ada_val
colnames(valori)=c("tree","tree_pru","gbm","randFor","glm","svm","ada")




















#--------------------------------------------------------
#---------------------CURVE ROC--------------------------
#--------------------------------------------------------





#pred.tree
#pred.tree_prune
#pred.gbm
#pred.rf
#pred.glm
#pred.svm
#pred.ada

#Per la curva ROC e l'area sotto la curva
#servono gli oggetti prediction e non i predict
pred.tree = predict( tree, testing[,-ncol(testing)], type="prob" ) 
pred.tree = prediction(pred.tree[, 2], testing$TARGET)

pred.tree_prune = predict( tree_prune, testing[,-ncol(testing)], type="prob" ) 
pred.tree_prune = prediction(pred.tree_prune[, 2], testing$TARGET)

pred.glm = predict(LG_full, testing[,-ncol(testing)], type="response" ) 
pred.glm = prediction(pred.glm, testing$TARGET)

pred.rf = predict(rf, testing[,-ncol(testing)], type="prob" ) 
pred.rf = prediction(pred.rf[,2], testing$TARGET)

pred.gbm = predict(gbmfit, testing[,-ncol(testing)], type="response",n.trees=select.M.iter ) 
pred.gbm = prediction(pred.gbm, testing$TARGET)

pred.svm = predict(svm_model, testing[,-ncol(testing)], type="prob" ) 
pred.svm = prediction(pred.svm, testing$TARGET)

pred.ada = predict(fit.ada, testing[,-ncol(testing)], type="prob" ) 
pred.ada = prediction(pred.ada[,2], testing$TARGET)




#AUC
AUC_tree=performance(pred.tree, "auc")@y.values[[1]]
AUC_tree_prune=performance(pred.tree_prune, "auc")@y.values[[1]]
AUC_glm=performance(pred.glm, "auc")@y.values[[1]]
AUC_rf=performance(pred.rf, "auc")@y.values[[1]]
AUC_gbm=performance(pred.gbm, "auc")@y.values[[1]]
AUC_svm=performance(pred.svm, "auc")@y.values[[1]]
AUC_ada=performance(pred.ada, "auc")@y.values[[1]]

AUC=c(AUC_tree,AUC_tree_prune,AUC_glm,AUC_rf,AUC_glm,AUC_gbm,AUC_svm,AUC_ada)

#aggiungo la nuova riga
valori=rbind(valori, AUC)
rownames(valori)=c("acc","sen","spe","AUC")


#PLOT CURVA ROC
plot(performance(pred.tree,'tpr','fpr'),col="red")
legend('bottomright', c("tree","tree_prune","gbm","randFor","glm","svm","ada"), lty=1, col=c('red', 'blue','green','brown','yellow','pink','black'), bty='n', cex=.75)
par(new=TRUE)
plot(performance(pred.tree_prune,'tpr','fpr'),col="blue")
par(new=TRUE)
plot(performance(pred.gbm,'tpr','fpr'),col="green")
par(new=TRUE)
plot(performance(pred.rf,'tpr','fpr'),col="brown")
par(new=TRUE)
plot(performance(pred.glm,'tpr','fpr'),col="yellow")
par(new=TRUE)
plot(performance(pred.svm,'tpr','fpr'),col="pink")
par(new=TRUE)
plot(performance(pred.ada,'tpr','fpr'),col="black")