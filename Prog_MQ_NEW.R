#caricamento dati assegnando una variabile chiamata data5

data5 = read.csv("IBMforecast.csv", header = TRUE, sep = ";")
#View(data5) #visualizza il file
#summary(data5) #riassunto dei dati a disposizione

##############codice alternativo###############################
#test = read.csv("testsetDJcomponents.csv", header = TRUE, sep = ";")
#train_idx <- fix(2:nrow(data),450,replace=FALSE)
#View(train_idx)

#View(test)
#summary(test)
###############################################################
#questa funzione divide i dati in 2 parti (le righe sono il nr di osservazioni e le colonne rappresentano le ns varibaili) per righe prendendo 
#in qst caso 2/3 delle osservazioni che usa come training set e il resto (1/3) lo usa come test set
#nel codice, la lettera "y" (y minuscola) è usata da R di default per rappresentare l'output (vatiabile dipendente) e 
#la lettera "Y" (Y maiuscola) è la variabile che noi abbiamo deciso di chiamare cosi e che usiamo come output e che si trova nel file data5
#il simbolo "$" prende quella determinata variabile (colonna) che noi vogliamo (usare come output)
partition5 <- createDataPartition(y=data5$Y,p=2/3,list=FALSE) #5 sta a signifare che si riferisce al file data5, come traccia per le numerose prove
#in partition5 abbiamo entrambi i sottoinsiemi del file data5 da usare come train e test. in seguito, specifichiamo quale dei 2 sottoinsiemi usaiamo
#come training e test set
training5 <- data5[partition5,] #comprende gli setssi dati di partition5 ma in qst modo lo esplicitiamo
#View(training5)
Validation5 <- data5[-partition5,] #tutto qll che non rientra nel training e che utilizziamo come test
#View(training5)# View(Validation1)
modfit5 <- train(Y~.,method="rpart",data=training5) # costruzione algoritmica dell'albero che verrà graficamente visualizzato in seguito
show(modfit5)
#  modfit <- train(Y ~ ., data=data, method="rf", tuneGrid=data.frame(mtry=3),trControl=trainControl(method="none"))
#modfitxy <- train(Y~.,method="rpart",data=train) 
#fancyRpartPlot(modfitxy$finalModel)
#plot(modfitxy) 

fancyRpartPlot(modfit5$finalModel) #visualizzazione grafica dell'albero ottenuto
plot(modfit5) #complexity parameter

train.pred5 <- predict(modfit5,newdata=training5)
table(train.pred5,training5$Y)
plot(train.pred5)

test.pred5 <- predict(modfit5,newdata=Validation5)
#test.pred <- predict(modfit,newdata=test)- test$Y
#test.pred <- predict(modfit,newdata=test,type = "raw") - test$Y
table(test.pred5,Validation5$Y)
plot(test.pred5)

#test.pred <- predict(modfit, data=test[,!colnames(train) %in% c("Y")], family="binomial")
#View(data=test[,!colnames(train) %in% c("Y")])
#table(test.pred,test$Y)
#predict(test.pred, newdata=test, type="response

options(stringsAsFactors = FALSE)

GroupA <- droplevels(training5[data5$Y=="training5",])

modfit4 <- train(Y~ .,method="rf",data=training5)


m <- standardize(lm(Y ~ ., data = training5))




dataset5 <- data5[complete.cases(data5$Y),]

dataset5 <- na.roughfix(data5)
View(dataset5)
data.rforest <- randomForest(data5$Y ~ ., data=training5, ntree=100, keep.forest=FALSE, importance=TRUE)

RandomForestPrediction = function(alpha){ training5,Validation5,myNtree
  +   test<-MPS[-d,]
  +   myNtree=1000
  +   myMtry=5
  +   myImportance=TRUE
  +   mod2 = randomForest(factor(m.Decision)~.,data=train,tree=myNtree,mtry=myMtry,importance=myImportance)
  +   fitted=predict(mod2,test,type="response")
  +   return(table(fitted,test$m.Decision))
  + }


