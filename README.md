# Prevision_Finacial_With_RF

![MIT license](https://img.shields.io/badge/license-MIT-blue.svg) ![Built with R](https://img.shields.io/badge/built%20with-R-green.svg)

### Prevision Finacial with method R_PARTITION and Random Forest.
Implemented with R Language

**Have an issue**
Head over to issue section to report any problem or bug to us and get help.

**Disclaimer**: Please Note that this is a research project. I am by no means responsible for any usage of this tool. Use on your own behalf.

[Check out our paper with what has been behind this work](http://bit.ly/2nfYThe)

Table of Contents
=================
* [Overview](#overview)
	* [Ensemble learning methods](#ensemble-learning-methods)
		* [Bagging](#11-bagging)
		* [Boosting](#12-boosting)
	* [Features of Random Forest](#features-of-random-forest)
	* [How random forests work](#How-random-forest)
* [Getting Started](#getting-started)
  * [Basic Installation on Ubuntu](#basic-installation-on-ubuntu)
  * [Basic Installation on Windows 10/Mac OSx](#basic-installation-on-windows-10mac-osx)
* Execute it
  * [How to run the program](#how-to-run-the-program)
  * [Explanation of the code](#explanation-of-the-code)
* [Extra Informations](#extra-informations)

## Overview
### Ensemble learning methods
The literary meaning of word ‘ensemble’ is group. Ensemble methods represent a great set of models used and applied principally to decision trees, or at least in this context, and
have the function to give to the initial method a better accuracy level and stability. In case of tree based models, ensemble learning methods are able, using different methods, to give more predictive power. The decision trees alone, cannot achieve the same prediction accuracy. This is not an absolute affirmation, as it depends on type of study one is doing, but in most part of cases this is true. One of the cases this is not verified is when we try to make a prediction through decision trees using uncorrelated features, or anyway, features that have not a predictive power on the dataset labels. An ensemble method is considered a supervised learning algorithm.not a predictive power on the dataset labels. An ensemble method is considered a supervised learning algorithm.

## Ensemble learning techniques:
### 1.1 Bagging
Bagging is a technique used to reduce the variance of our predictions.
It consists on combining different results obtained by applying a decision classifier to different sub-samples of the dataset.
The following image will give a better idea on how it works

![Bagging Method](https://www.analyticsvidhya.com/wp-content/uploads/2015/07/bagging.png)
 
 A theoretical explanation of Bagging method:
Given a training set X, where X = x1, ..., xn, where 𝑥𝑖, 𝑖 = 1,2, ... , 𝑛 is our 𝑖𝑡h sample unit
with respective responses Y = y1, ..., yn, bagging method repeatedly (B times) selects a random sample with replacement of the training set and fits trees to these samples:
 For b = 1, ..., B:
 1. This method consists on sampling, with replacement, n training examples from 𝑋, 𝑌; call these 𝑋𝑏, 𝑌𝑏.
2. Then, the method grows a classification or regression tree 𝑓𝑏 on 𝑋𝑏, 𝑌𝑏.

If we consider the original training set, and create decision trees on that, single trees suffer the noise effect. They are highly sensitive to that. And the fact that training set is chosen randomly, just gives to this affirmation more value. In case of growing multiple trees, where each one of them is based on bagging method, they don’t suffer that anymore as they are independent from each other. In the first case, they are strongly correlated. If the partition process of our data is not random, in this case it is possible to have same tree more time, as it is deterministic process. The learner chooses the number of samples to create. It is a free parameter depending on how much he knows the argument, his
experience on this kind of study and other factors. Typically, a few hundred to several thousand trees are used, depending on the size and nature of the training set. It is possible to find the number of trees B that gives to us the vest result using cross-validation, or trying to adapt this number basing the decision on out-of-bag error observation: out-of-bag error gives us the mean prediction error on each training sample 𝑥𝑖. He does that using only trees where the 𝑥𝑖 was not present in their bootstrap sample.
Normally, training and testing error tend to have same level after the right number of trees have been grown, where the trees right number changes and depends on data type.
So, shortly, these are the steps that we must follow to reduce variance with bagging method:

1. Create Multiple Data sets:
o Through with replacement method we create different samples on the original data and, this way, we have new datasets
o New datasets are composed from random rows (random single inputs
sampled and not all of them) and columns (features)
o Considering in tree grow process only a fraction of our data helps in making robust models, that takes to over-fitting condition in a more difficult way
2. Build multiple classifiers:
o On each new dataset a new classifier is created. We will have as classifiers as the number of random samples
o Generally, same classifier is modeled on each data set and, based on that, we made predictions
3. Combine classifiers:
o In the next step, we make predictions using all the classifiers, and we combine them using a mean, median or mode value depending on the problem
o The results obtained that way are usually more reliable than a single decision tree or other classifier type

Higher number of models are always better or may give similar performance than lower numbers. It can be theoretically shown that the variance of the combined predictions is reduced to 1/n (n is the number of classifiers) of the original variance.
There are various implementations of bagging models. Random forest is one of them and I will discuss it below.


### 1.2 Boosting
‘Boosting’ method belong to a particular family of algorithms that are able to convert a weak learner on a strong learner, through a specific data treatment.
 This is an example on spam mail detection that better explains the concept of boosting method. The problem is to understand if an e-mail is a Spam mail or not. How can we do it? The most used method to do that is to follow the below criteria.
If:
 1. An e-mail presents promotion images, it belongs to the category “It’s a Spam”
 2. If an e-mail is composed by only links, it belongs to the category “It’s a Spam”
3. If an e-mail presents written models like “You won a prize money of $ xxxxxx”, it
 belongs to the category “It’s a Spam”
 4. If an e-mail takes origin from Sapienza official domain “www.uniroma1.it”, it
belongs to the category “Not a Spam”
 5. If we receive an e-mail from an unknown source, it belongs to the category “Not a
Spam”
  
  Above, we have defined some rules that can classify an email into ‘Spam’ or ‘Not a Spam’. But considering these rules only is not strong enough way to make a good prediction and to classify an email into ‘Spam’ or ‘Not a Spam’.
Therefore, these rules are called as weak learner. It is possible, as I mentioned above, to convert a weak learner into strong learner using methods like:
 1. Weighted average value
 2. Considering all prediction with a higher vote

 For example, above, I have defined 5 weak learners. Out of these 5, 3 of them belong to the
 category “It’s a Spam” and 2 belong to the category “Not a Spam”. Obviously, taking into consideration the above results, I will consider a mail as “It’s a Spam”.


To avoid that, we need boosting. It tries to convert a set of weak rules into a strong rule, that represents our strong learner. An immediate question is “How boosting identifies weak rules”? He does that applying multiple algorithms that belong to the base learning. Each of them generates a weak learner, and combining all these weak learners formed by machine learning algorithms, boosting method outputs a strong rule. This is an iterative process for each new base machine learning method application.
Another question might be: ‘How do we choose different distribution for each round’?
To do that, we must follow some steps:

1. An equal weight is assigned to each distribution from the base machine learning 
2.  At this point, if there are errors on prediction process, a higher attention is paid to
the error cause
3. An iteration is done until we achieve the desired result or, anyway, at least a better prediction
After that, a combination of all these results is done and, hopely, a better result is obtained. This method pays more attention and assign a higher weight to the cases which present a higher miss-classification error.
   

### Features of Random Forest:
Random Forests are considered one of the best prediction models. They have really good accuracy and stability.
Two methods of feature selection are provided from them: mean decrease impurity and mean decrease accuracy.

 1. Mean decrease impurity Random forest consists of a number of decision trees. The best split condition is choose taking in consideration the impurity concept.
It depends if we are talking about classification or regression problem. I already explained that in the previous chapter, but Random Forest are based on impurity measure too, as they are an ensemble of decision trees. For a decision tree we can compute the impurity decrease each feature selection gives to the tree. In the forest case, we can average the impurity decrease for each feature and then rank them, basing this rank on the impurity measure.
A problem that can exist of feature importance measure is about very correlated features. If there are two (or more) correlated features, the model selects only one of them as the most important and, doing this way, the effect of other features results lower than it really is. They will not be considered as important variables even if they actually are. The problem of that regards the data interpretation. We could think that one (or more) variables are not relevant for the output, and this is not true. We will have a wrong idea on the feature (s) and output behavior. It will not be a complete and efficient study of the specific phenomenon.
 
 2. Mean decrease accuracy Another way to calculate feature importance
    is measuring directly which is the feature impact on the accuracy of
    the model. The basic idea is to change all values of that feature
    during the fitting process, and see which impact different values of
    the feature have on accuracy. The less important is a variable, the
    lower will be the effect of this permutation.

 


----------


It is unexcelled in accuracy among current algorithms. It runs efficiently on large data bases. It can handle thousands of input variables without variable deletion. It gives estimates of what variables are important in the classification. It generates an internal unbiased estimate of the generalization error as the forest building progresses. It has an effective method for estimating missing data and maintains accuracy when a large proportion of the data are missing. It has methods for balancing error in class population unbalanced data sets. Generated forests can be saved for future use on other data. Prototypes are computed that give information about the relation between the variables and the classification. It computes proximities between pairs of cases that can be used in clustering, locating outliers, or (by scaling) give interesting views of the data. The capabilities of the above can be extended to unlabeled data, leading to unsupervised clustering, data views and outlier detection. It offers an experimental method for detecting variable interactions. Remarks Random forests does not overfit. You can run as many trees as you want. It is fast. Running on a data set with 50,000 cases and 100 variables, it produced 100 trees in **11 minutes** on a 800Mhz machine. For large data sets the major memory requirement is the storage of the data itself, and three integer arrays with the same dimensions as the data. If proximities are calculated, storage requirements grow as the number of cases times the number of trees.



## Getting started

### Terminal Installation on Ubuntu:

```bash
0   Ubuntu 16.04 LTS
1	sudo apt-get update
2	sudo apt-get install r-base
3	sudo apt-get install r-base-dev
4	sudo apt-get install gdebi-core
5	wget https://download1.rstudio.org/rstudio-1.1.419-amd64.deb
6	sudo gdebi rstudio-1.1.419-amd64.deb
7	rm rstudio-1.1.419-amd64.deb
```

### Basic Installation on Windows 10
```bash
1	https://www.rstudio.com/products/rstudio/download/#download
2	Install version FREE and choose the one for your operating system
3	Then install the package
```
### Basic Installation on Mac OSX
```bash
0   Install R binary files from here: https://cran.r-project.org/bin/macosx/
1	https://www.rstudio.com/products/rstudio/download/#download
2	Install version FREE and choose the one for your operating system
3	Then install the package
```



### How to run the program


 Open File Prog_+_Dataset/Prog_New_Old/Prevision_Finacial_with_RF.R
 
```
 Choose the window Packages and the you have to select:
	Caret
	Coin
	E1071
	Foreach
	Rattle
	Gplot2
	Iterators
	Labelling
	Ime6
	Magrittr
	Maptree
	MatrixModels
	ModelMetrics
	Modeltools
	Multcomp
	Munsell
	Mvtnorm
	randomForest
	randomForestSRC
	Tree
	Cluster
	Rpart
```	

NOW you need to select the folder where all your dataset are stored.
Choose the Window Files on the right and then open the directory from icon with 3 point on the right for the dataset.

At this point we should have RStudio ready, packages and dataset folder selected, we can now proceed to the executing of the code.

###### NB!! Click **CTRL+ENTER** to execute each command.


### Explanation of the code
 1. **Import your Dataset:**
 
	`	data5 = read.csv("IBMforecast.csv", header =TRUE, sep = ";") `
	
 2. **View the var data5:**  	`View(data5) ` 	
	 This function divides the data in 2 groups (raw: n°of observation, col: n°of our vars), in
    this case the raw equivals at 2/3 of observation wich utilizes the
    training set and other part of 1/3 for test set. 	y: default var
    wich reppresent output. 	Y: var introduced by us for output wich is
    in file of data5 	Get Y (var col) wich we want to use 
    	`	partition5 <-  createDataPartition(y=data5$Y,p=2/3,list=FALSE) `

 3. **Explicit the training group**
	```
	training5 <- data5[partition5,]
	View(training5) 
	```
 4. **Explicit the test group**
	```
	Validation5 <- data5[-partition5,] 
	View(Validation5)
	```	
 5. **Algorithmic Building of the tree  and graphical show of the result using the rpart method**

	**Rpart mode**: Rpart (recursive partition) Any observation with values for the dependent variable and at least one independent variable will participate in the modeling.
	```
	modfit5 <- train(Y~.,method="rpart",data=training5)
	show(modfit5)
	options(stringsAsFactors = FALSE)
	GroupA <- droplevels(training5[data5$Y=="training5",])
	```
	
 6. **Algorithmic Building of the tree using the Random Forest method**
	```
	modfit4 <- train(Y~ .,method="rf",data=training5)
	m <- standardize(lm(Y ~ ., data = training5))
	dataset5 <- data5[complete.cases(data5$Y),]
	dataset5 <- na.roughfix(data5)
	View(dataset5)
	```
	
 7. **Complete the Tree with Random Forest Prediction method**
	```
	data.rforest <- randomForest(data5$Y ~ ., data=training5, ntree=100, keep.forest=FALSE, importance=TRUE)
	RandomForestPrediction = function(alpha){ training5,Validation5,myNtree
	  +   test<-MPS[-d,]
	  +   myNtree=1000
	  +   myMtry=5
	  +   myImportance=TRUE
	  +   mod2 = randomForest(factor(m.Decision)~.,data=train,tree=myNtree,mtry=myMtry,importance=myImportance)
	  +   fitted=predict(mod2,test,type="response")
	  +   return(table(fitted,test$m.Decision))
	  + }
	  ```


----------


***REMEMBER:***
	To execute the codes you have to click 
	in the same moment CTRL+ENTER and then you
	have to wait the execution until the code 
	is done.
	You can see the status by red icon of 
	console.

## Extra Informations

### R-STUDIO 1.1.419

64-bit system is a requirement for current versions of R-STUDIO for a better stability.

---
###### Have Fun & Feel Free to report any issues  or help to our issues section
---
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTExMTQzNzIzNzEsMTYxOTk1NzAzNywtNz
czNzc3NzM1LDIxMTg5MDUzNDcsLTc3Mzc3NzczNSwyMTE4OTA1
MzQ3LC03NzM3Nzc3MzUsMjExODkwNTM0Nyw2NjEwMzk0NywxNj
k2MTEzNjIxLDY2MTAzOTQ3LDE2OTYxMTM2MjEsNjYxMDM5NDcs
MTY5NjExMzYyMSw2NjEwMzk0NywtMjAzNzc1Nzk2MiwtMTcyND
A4MjkwMCwtNTI3MjM4NjAyLDIwMzkzOTkzNDMsLTcyMDgyMjQx
NywxNTE0MzU2MjQxLDE1NzQwODI0NjRdfQ==
-->